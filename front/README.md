# Canang Sagacity Finance

SagaCity Finance

## Setup

* Ensure you have Node 4.7.0 and NPM 4.0.5+ installed.
* Install Angular CLI `npm i -g angular-cli@1.0.0-beta.24`
* Install Typescript 2.0+ `npm i -g typescript`
* Install TSLint `npm install -g tslint`
* Install Protractor for e2e testing `npm install -g protractor`
* Install Node packages `npm i`
* Update Webdriver `webdriver-manager update` and `./node_modules/.bin/webdriver-manager update`
* Run local build `ng serve`

