import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/filter';
import {Headers, RequestOptions} from '@angular/http';
import {Http, Response} from '@angular/http';
import {Student} from "../app/academic/students/student.interface";
import {environment} from "../environments/environment";
import {Course} from "../app/academic/courses/course.interface";
import {Faculty} from "../app/academic/faculties/faculty.interface";

@Injectable()
export class AcademicService {

  constructor(private http: Http) {
  }

  findStudents(): Observable<Student[]> {
    let headers = new Headers();
    let options = new RequestOptions({headers: headers});
    return this.http.get(environment.endpoint + '/api/academic/students', options)
      .map((res: Response) => {
        return <Student[]>res.json();
      });
  }

  findStudentByStudentNo(studentNo: string):Observable<Student> {
    let headers = new Headers();
    let options = new RequestOptions({headers: headers});
    return this.http.get(environment.endpoint + '/api/academic/students/' + studentNo, options)
      .map((res: Response) => {
        return <Student>res.json();
      });
  }
  findCourses(): Observable<Course[]> {
    let headers = new Headers();
    let options = new RequestOptions({headers: headers});
    return this.http.get(environment.endpoint + '/api/academic/courses', options)
      .map((res: Response) => {
        return <Course[]>res.json();
      });
  }

  findCourseByCourseNo(courseNo: string):Observable<Course> {
    let headers = new Headers();
    let options = new RequestOptions({headers: headers});
    return this.http.get(environment.endpoint + '/api/academic/courses/' + courseNo, options)
      .map((res: Response) => {
        return <Course>res.json();
      });
  }

  findFaculties(): Observable<Faculty[]> {
    let headers = new Headers();
    let options = new RequestOptions({headers: headers});
    return this.http.get('data/faculties.json', options)
      .map((res: Response) => {
        return <Faculty[]>res.json();
      });
  }
}
