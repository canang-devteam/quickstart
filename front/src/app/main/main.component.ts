import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'pams-main',
  templateUrl: './main.component.html',
})

export class MainComponent {

  private _router: Router;

  routes: Object[] = [{
    title: 'Manage Students',
    route: '/academic/students',
    icon: 'home',
  },{
    title: 'Manage Courses',
    route: '/academic/courses',
    icon: 'home',
  }
  ];

  constructor(router: Router) {
    this._router = router;
  }

  logout(): void {
    this._router.navigate(['/login']);
  }
}
