import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms';
import {CovalentHttpModule} from "@covalent/http";
import {CovalentCoreModule} from '@covalent/core';

import {MainComponent} from './main/main.component';
import {appRoutes, appRoutingProviders} from './app.routes';
import {AcademicService} from "../services/academic.service";
import {AppComponent} from "./app.component";
import {AcademicModule} from "./academic/index";
import {HomeModule} from "./home/index";


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
  ],
  imports: [
    // our module
    HomeModule.forRoot(),
    AcademicModule.forRoot(),

    // deps module
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CovalentCoreModule.forRoot(),
    CovalentHttpModule.forRoot(),
    appRoutes,
  ],
  providers: [
    appRoutingProviders,
    AcademicService,
  ],
  entryComponents: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
