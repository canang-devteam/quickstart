import {Routes, RouterModule} from '@angular/router';

import {MainComponent} from './main/main.component';

// child routes
import {AcademicModuleRoutes} from "./academic/academic-module.routes";
import {HomeModuleRoutes} from "./home/home-module.routes";

const routes: Routes = [
  {
    path: '', component: MainComponent,
    children: [
      ...HomeModuleRoutes,
      ...AcademicModuleRoutes
    ]
  },
];

export const appRoutingProviders: any[] = [];

export const appRoutes: any = RouterModule.forRoot(routes);
