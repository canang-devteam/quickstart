import {Component, Output, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {AcademicService} from "../../services/academic.service";

@Component({
  selector: 'pam-home',
  templateUrl: './home.page.html',
})

export class HomePage implements OnInit {
  private _router: Router;
  private _route: ActivatedRoute;
  private _academicService: AcademicService;

  private items: Object[];

  constructor(router: Router,
              route: ActivatedRoute,
              academicService: AcademicService) {
    this._router = router;
    this._route = route;
    this._academicService = academicService;
  }

  ngOnInit(): void {
    this.items = [{
      title: 'Dashboard',
      route: '/dashboard',
      icon: 'dashboard',
      color: 'red-500',
      description: '',
    },
      {
        title: 'Intake',
        route: '/intake',
        icon: 'assignment',
        color: 'red-500',
        description: '',
      },
      {
        title: 'Academic',
        route: '/academic',
        icon: 'assignment',
        color: 'red-500',
        description: '',
      },
      {
        title: 'Account',
        route: '/account',
        icon: 'assignment',
        color: 'red-500',
        description: '',
      }
    ];
  }
}
