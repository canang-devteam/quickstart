import {Routes, RouterModule} from '@angular/router';
import {HomePage} from "./home.page";


// Route Configuration
export const HomeModuleRoutes: Routes = [
  {path: '', component: HomePage}
];
