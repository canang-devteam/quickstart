export interface Course {
  courseNo:string;
  title:string;
  credit:number;
}
