import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {AcademicService} from "../../../services/academic.service";
import {Course} from "./course.interface";
import {Student} from "../students/student.interface";


@Component({
  selector: 'pams-course-detail',
  templateUrl: './course-detail.page.html',
})
export class CourseDetailPage implements OnInit {

  private _router: Router;
  private _route: ActivatedRoute;
  private _academicService: AcademicService;

  private course: Course = <Course>{};
  private students: Student[] = <Student[]>[];

  constructor(router: Router,
              route: ActivatedRoute,
              academicService: AcademicService) {
    this._router = router;
    this._route = route;
    this._academicService = academicService;
  }

  ngOnInit(): void {
    this._route.params.subscribe((params: { courseNo: string }) => {
      let courseNo: string = params.courseNo;
      this.loadCourse(courseNo);
      this.loadStudents();
    });
  }

  private loadCourse(courseNo: string) {
    this._academicService.findCourseByCourseNo(courseNo)
      .subscribe((course: Course) => {
      this.course = course;
    });
  }

  private loadStudents() {
    this._academicService.findStudents()
      .subscribe((students: Student[]) => {
      this.students = students;
    });
  }

  goBack(): void {
    this._router.navigate(['/academic']);
  }
}

