import {Routes, RouterModule} from '@angular/router';
import {CourseDetailPage} from "./course-detail.page";
import {CourseListPage} from "./course-list.page";

// Route Configuration
export const CourseRoutes: Routes = [
  {path: 'academic/courses', component: CourseListPage},
  {path: 'academic/courses/:courseNo', component: CourseDetailPage},
];
