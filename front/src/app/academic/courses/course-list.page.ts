import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {AcademicService} from "../../../services/academic.service";
import {Course} from "./course.interface";

@Component({
  selector: 'pams-course-list',
  templateUrl: './course-list.page.html',
})
export class CourseListPage implements OnInit {

  private _router: Router;
  private _route: ActivatedRoute;
  private _academicService: AcademicService;

  private courses: Course[] = <Course[]>[];

  constructor(router: Router,
              route: ActivatedRoute,
              academicService: AcademicService) {
    this._router = router;
    this._route = route;
    this._academicService = academicService;
  }

  ngOnInit(): void {
    this.loadCourses();
  }

  loadCourses(): void {
    this._academicService.findCourses()
      .subscribe((courses: Course[]) => {
        this.courses = courses;
      });
  }

  goBack(route: string): void {
    this._router.navigate(['/academic']);
  }
}

