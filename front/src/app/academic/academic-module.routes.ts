import {Routes, RouterModule} from '@angular/router';
import {StudentRoutes} from "./students/student.routes";
import {AcademicPage} from "./academic.page";
import {CourseRoutes} from "./courses/course.routes";
import {FacultyRoutes} from "./faculties/faculty.routes";


// Route Configuration
export const AcademicModuleRoutes: Routes = [
    {path: 'academic', component: AcademicPage},
  ...StudentRoutes,
  ...CourseRoutes,
  ...FacultyRoutes,
];
