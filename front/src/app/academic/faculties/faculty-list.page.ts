import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {AcademicService} from "../../../services/academic.service";
import {Faculty} from "./faculty.interface";

@Component({
  selector: 'pams-faculty-list',
  templateUrl: './faculty-list.page.html',
})
export class FacultyListPage implements OnInit {

  private _router: Router;
  private _route: ActivatedRoute;
  private _academicService: AcademicService;

  private faculties: Faculty[] = <Faculty[]>[];

  constructor(router: Router,
              route: ActivatedRoute,
              academicService: AcademicService) {
    this._router = router;
    this._route = route;
    this._academicService = academicService;
  }

  ngOnInit(): void {
    this.loadFaculties();
  }

  loadFaculties(): void {
    this._academicService.findFaculties()
      .subscribe((faculties: Faculty[]) => {
        this.faculties = faculties;
      });
  }

  goBack(route: string): void {
    this._router.navigate(['/academic']);
  }
}

