import {Routes, RouterModule} from '@angular/router';
import {FacultyListPage} from "./faculty-list.page";

// Route Configuration
export const FacultyRoutes: Routes = [
  {path: 'academic/faculties', component: FacultyListPage},
];
