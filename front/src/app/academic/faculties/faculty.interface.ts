export interface Faculty {
    id:number;
    code:string;
    name:string;
}