import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {AcademicService} from "../../../services/academic.service";
import {Student} from "./student.interface";

@Component({
  selector: 'pams-student-list',
  templateUrl: './student-list.page.html',
})
export class StudentListPage implements OnInit {

  private _router: Router;
  private _route: ActivatedRoute;
  private _academicService: AcademicService;

  private students: Student[] = <Student[]>[];

  constructor(router: Router,
              route: ActivatedRoute,
              academicService: AcademicService) {
    this._router = router;
    this._route = route;
    this._academicService = academicService;
  }

  ngOnInit(): void {
    this.loadStudents();
  }

  loadStudents(): void {
    this._academicService.findStudents()
      .subscribe((students: Student[]) => {
        this.students = students;
      });
  }

  goBack(route: string): void {
    this._router.navigate(['/academic']);
  }
}

