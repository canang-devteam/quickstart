import {Routes, RouterModule} from '@angular/router';
import {StudentDetailPage} from "./student-detail.page";
import {StudentListPage} from "./student-list.page";

// Route Configuration
export const StudentRoutes: Routes = [
  {path: 'academic/students', component: StudentListPage},
  {path: 'academic/students/:studentNo', component: StudentDetailPage},
];
