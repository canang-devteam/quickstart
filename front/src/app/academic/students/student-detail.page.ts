import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {AcademicService} from "../../../services/academic.service";
import {Student} from "./student.interface";
import {Course} from "../courses/course.interface";


@Component({
  selector: 'pams-student-detail',
  templateUrl: './student-detail.page.html',
})
export class StudentDetailPage implements OnInit {

  private _router: Router;
  private _route: ActivatedRoute;
  private _academicService: AcademicService;

  private student: Student = <Student>{};
  private courses: Course[] = <Course[]>[];

  constructor(router: Router,
              route: ActivatedRoute,
              academicService: AcademicService) {
    this._router = router;
    this._route = route;
    this._academicService = academicService;
  }

  ngOnInit(): void {
    this._route.params.subscribe((params: { studentNo: string }) => {
      let studentNo: string = params.studentNo;
      this.loadStudent(studentNo);
      this.loadCourses();
    });
  }

  private loadStudent(studentNo: string) {
    this._academicService.findStudentByStudentNo(studentNo)
      .subscribe((student: Student) => {
      this.student = student;
    });
  }

  private loadCourses() {
    this._academicService.findCourses()
      .subscribe((courses: Course[]) => {
      this.courses = courses;
    });
  }

  goBack(): void {
    this._router.navigate(['/academic']);
  }
}

