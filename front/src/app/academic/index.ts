import {NgModule, ModuleWithProviders} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms';
import {appRoutes, appRoutingProviders} from '../app.routes';

import {CovalentCoreModule} from '@covalent/core';
import {AcademicService} from "../../services/academic.service";
import {AcademicPage} from "./academic.page";
import {StudentListPage} from "./students/student-list.page";
import {StudentDetailPage} from "./students/student-detail.page";
import {CourseListPage} from "./courses/course-list.page";
import {CourseDetailPage} from "./courses/course-detail.page";
import {FacultyListPage} from "./faculties/faculty-list.page";

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    CovalentCoreModule.forRoot(),
    appRoutes,
  ],
  declarations: [
    AcademicPage,
    StudentListPage,
    StudentDetailPage,
    CourseListPage,
    CourseDetailPage,
    FacultyListPage,
  ],
  exports: [],
})
export class AcademicModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AcademicModule,
      providers: [
        appRoutingProviders,
        AcademicService,
      ],
    };
  }
}
