import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';


@Component({
  selector: 'pams-academic-page',
  templateUrl: './academic.page.html',
})

export class AcademicPage implements OnInit {

  private _router: Router;
  private _route: ActivatedRoute;

  constructor(router: Router,
              route: ActivatedRoute) {
    this._router = router;
    this._route = route;
  }

  ngOnInit(): void {
    this._route.params.subscribe(() => {
    });
  }
}
