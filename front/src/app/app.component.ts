import {Component, ViewContainerRef} from '@angular/core';

import {DomSanitizer} from '@angular/platform-browser';
import {MdIconRegistry} from '@angular/material';

@Component({
  selector: 'pams-app',
  templateUrl: './app.component.html',
})
export class AppComponent {

  constructor(iconRegistry: MdIconRegistry, domSanitizer: DomSanitizer) {
    iconRegistry.addSvgIconInNamespace('assets', 'logo', domSanitizer.bypassSecurityTrustResourceUrl('/assets/icons/logo.svg'));
  }

}
