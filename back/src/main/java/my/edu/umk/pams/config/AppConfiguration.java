package my.edu.umk.pams.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = {
        "my.edu.umk.pams",
}
)
@Import({
        SwaggerConfig.class
})
@PropertySource("classpath:application.properties")
public class AppConfiguration {
}
