package my.edu.umk.pams.academic.service;

import my.edu.umk.pams.web.vo.Course;
import my.edu.umk.pams.web.vo.Student;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("academicService")
public class AcademicServiceImpl implements AcademicService {


    public static List<Student> students = new ArrayList<Student>();
    {
        students.add(new Student("1234", "Ahmad Razak", "ahmad.razak@umk.edu.my"));
        students.add(new Student("1235", "Ahmad Razali", "ahmad.razali@umk.edu.my"));
        students.add(new Student("1236", "Siti Soraya", "siti.soraya@umk.edu.my"));
        students.add(new Student("1237", "Rizal Mokthar", "rizal.mokhtar@umk.edu.my"));
    }

    public static List<Course> courses = new ArrayList<Course>();
    {
        courses.add(new Course("MS1234", "Introduction to Programming", 2));
        courses.add(new Course("MS1235", "Introduction to Civilization", 3));
        courses.add(new Course("MS1236", "Once a upon a time in China", 2));
        courses.add(new Course("MS1237", "Philosophy of Building Material", 4));
    }

    @Override
    public Student findStudentByStudentNo(String studentNo) {
        return students.get(0);
    }

    @Override
    public List<Student> findStudents() {
        return students;
    }

    @Override
    public Course findCourseByCourseNo(String courseNo) {
        return courses.get(0);
    }

    @Override
    public List<Course> findCourses() {
        return courses;
    }
}
