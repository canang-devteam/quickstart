package my.edu.umk.pams.academic.service;

import my.edu.umk.pams.web.vo.Course;
import my.edu.umk.pams.web.vo.Student;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author team canang
 */
public interface AcademicService {

    public Student findStudentByStudentNo(String studentNo);

    public List<Student> findStudents();

    public Course findCourseByCourseNo(String courseNo);

    public List<Course> findCourses();

}
