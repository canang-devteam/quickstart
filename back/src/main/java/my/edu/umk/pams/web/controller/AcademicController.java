package my.edu.umk.pams.web.controller;

import my.edu.umk.pams.academic.service.AcademicService;
import my.edu.umk.pams.web.vo.Course;
import my.edu.umk.pams.web.vo.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/academic")
public class AcademicController {

    @Autowired
    private AcademicService academicService;

    @RequestMapping(value = "/students", method = RequestMethod.GET)
    public ResponseEntity<List<Student>> findStudents() {
        return new ResponseEntity<List<Student>>(academicService.findStudents(), HttpStatus.OK);
    }

    @RequestMapping(value = "/students/{studentNo}", method = RequestMethod.GET)
    public ResponseEntity<Student> findStudentByStudentNo() {
        return new ResponseEntity<Student>(academicService.findStudents().get(0), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    public ResponseEntity<List<Course>> findCourses() {
        return new ResponseEntity<List<Course>>(academicService.findCourses(), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses/{courseNo}", method = RequestMethod.GET)
    public ResponseEntity<Course> findCourseByCourseNo() {
        return new ResponseEntity<Course>(academicService.findCourses().get(0), HttpStatus.OK);
    }

}
