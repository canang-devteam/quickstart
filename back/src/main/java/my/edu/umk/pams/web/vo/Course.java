package my.edu.umk.pams.web.vo;

/**
 * @author team canang
 */
public class Course {
    private String courseNo;
    private String title;
    private Integer credit;

    public Course(String courseNo, String title, Integer credit) {
        this.courseNo = courseNo;
        this.title = title;
        this.credit = credit;
    }

    public String getCourseNo() {
        return courseNo;
    }

    public void setCourseNo(String courseNo) {
        this.courseNo = courseNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }
}
