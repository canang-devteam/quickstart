package my.edu.umk.pams.stage;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.integration.spring.JGivenStage;
import io.jsonwebtoken.lang.Assert;
import my.edu.umk.pams.academic.service.AcademicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@JGivenStage
public class ThenEnrollmentIsAdded extends Stage<ThenEnrollmentIsAdded> {

    private static final Logger LOG = LoggerFactory.getLogger(ThenEnrollmentIsAdded.class);

    @Autowired
    private AcademicService academicService;

    public void my_enrollment_count_has_increased(){
        LOG.debug("my enrollment count has increased");
        Assert.notNull(null);
    }
}
