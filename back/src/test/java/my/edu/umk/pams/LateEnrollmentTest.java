package my.edu.umk.pams;

import com.tngtech.jgiven.annotation.ScenarioStage;
import com.tngtech.jgiven.integration.spring.SpringScenarioTest;
import my.edu.umk.pams.stage.GivenIAmStudent;
import my.edu.umk.pams.stage.ThenAccountIsCharged;
import my.edu.umk.pams.stage.ThenLateEnrollmentIsAdded;
import my.edu.umk.pams.stage.WhenEnrollInSection;
import org.junit.Test;

public class LateEnrollmentTest extends SpringScenarioTest<GivenIAmStudent, WhenEnrollInSection, ThenLateEnrollmentIsAdded> {

    @ScenarioStage
    private ThenAccountIsCharged additionalThen;

    @Test
    public void testEnrollment() {
        given().I_am_a_student();
        when().I_late_enroll_in_section();
        then().my_enrollment_count_has_increased();
        additionalThen.and().my_account_is_charged();
    }
}
