package my.edu.umk.pams.stage;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.integration.spring.JGivenStage;
import my.edu.umk.pams.academic.service.AcademicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@JGivenStage
public class ThenLateEnrollmentIsAdded extends Stage<ThenLateEnrollmentIsAdded> {

    private static final Logger LOG = LoggerFactory.getLogger(ThenLateEnrollmentIsAdded.class);

    @Autowired
    private AcademicService academicService;

    public void my_enrollment_count_has_increased(){
        LOG.debug("my enrollment count has increased");
    }

    public void my_account_is_charged() {
        LOG.debug("my account is charged");
    }
}
