package my.edu.umk.pams.stage;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.integration.spring.JGivenStage;
import my.edu.umk.pams.academic.service.AcademicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@JGivenStage
public class GivenIAmStudent extends Stage<GivenIAmStudent> {

    private static final Logger LOG = LoggerFactory.getLogger(GivenIAmStudent.class);

    @Autowired
    private AcademicService academicService;

    public void I_am_a_student(){
        LOG.debug("i'm logging in as student");
    }
}
