package my.edu.umk.pams.stage;

import com.tngtech.jgiven.Stage;
import my.edu.umk.pams.academic.service.AcademicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author team canang
 */
public class ThenAccountIsCharged extends Stage<ThenAccountIsCharged> {

    private static final Logger LOG = LoggerFactory.getLogger(ThenAccountIsCharged.class);

    @Autowired
    private AcademicService academicService;

    public void my_account_is_charged(){
        LOG.debug("my account is charged");
    }
}
