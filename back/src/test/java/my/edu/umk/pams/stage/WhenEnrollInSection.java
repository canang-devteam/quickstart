package my.edu.umk.pams.stage;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.integration.spring.JGivenStage;
import my.edu.umk.pams.academic.service.AcademicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@JGivenStage
public class WhenEnrollInSection extends Stage<WhenEnrollInSection> {

    private static final Logger LOG = LoggerFactory.getLogger(WhenEnrollInSection.class);

    @Autowired
    private AcademicService academicService;

    public void I_late_enroll_in_section(){
        LOG.debug("when i enroll in section");
    }
}
