package my.edu.umk.pams;

import com.tngtech.jgiven.integration.spring.SpringScenarioTest;
import my.edu.umk.pams.stage.GivenIAmStudent;
import my.edu.umk.pams.stage.ThenEnrollmentIsAdded;
import my.edu.umk.pams.stage.WhenEnrollInSection;
import org.junit.Test;

public class EnrollmentTest extends SpringScenarioTest<GivenIAmStudent, WhenEnrollInSection, ThenEnrollmentIsAdded> {

    @Test
    public void testEnrollment() {
        given().I_am_a_student();
        when().I_late_enroll_in_section();
        then().my_enrollment_count_has_increased();
    }
}
