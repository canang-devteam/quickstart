package my.edu.umk.pams;

import my.edu.umk.pams.config.TestAppConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = TestAppConfiguration.class)
public class SampleTest {

    private static final Logger LOG = LoggerFactory.getLogger(SampleTest.class);

    @Before
    public void setup() {
    }

    @Test
    @Rollback(true)
    public void test() {
        LOG.debug("test is running");
    }
}